-- Floating windows backgroud
vim.api.nvim_set_hl(0, 'NormalFloat', { ctermfg=12, gui=bold, guifg=Blue })

-- Airline
vim.api.nvim_set_var('airline#extensions#tabline#enabled', 1)
vim.api.nvim_set_var('airline#extensions#tabline#left_sep', '')
vim.api.nvim_set_var('airline#extensions#tabline#right_sep', '')
vim.api.nvim_set_var('airline_left_sep', '')
vim.api.nvim_set_var('airline_right_sep', '')

-- Rest Console
vim.g.vrc_set_default_mapping = 0
vim.g.vrc_response_default_content_type = 'application/json'
vim.g.vrc_auto_format_response_patterns = {
  json = 'python3 -m json.tool'
}
vim.g.vrc_show_command = 1

-- Neogit

