local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

vim.g.mapleader = ' '

local plugins = {
  -- Adding my own plugins
  'vifm/vifm.vim',
  {
    'vim-airline/vim-airline',
    dependencies = {
        'tpope/vim-fugitive' -- To present the git branch
    }
  },
  'vim-airline/vim-airline-themes',
  {
    'neoclide/coc.nvim',
    branch='release',
    build = ':CocInstall coc-explorer'
  },
  'instant-markdown/vim-instant-markdown',
  {
    "NeogitOrg/neogit",
    dependencies = {
      "nvim-lua/plenary.nvim",         -- required
      "sindrets/diffview.nvim",        -- optional - Diff integration
      "nvim-telescope/telescope.nvim",
      "nvim-tree/nvim-web-devicons"
    },
    config = true
  },
  'diepm/vim-rest-console',
  'voldikss/vim-floaterm',
  {
    "folke/which-key.nvim",
    config = function()
      require('mappings.whichkey').init()
    end
  },
  {
    'nvim-treesitter/nvim-treesitter',
    build = ':TSUpdate'
  },
  {
    'nvim-telescope/telescope.nvim', version = '0.1.5',
    dependencies = { {'nvim-lua/plenary.nvim'} }
  }
}

require("lazy").setup(plugins, {})
