local M = {}

function M.init()
  local whichkey = require "which-key"

  local conf = {
    window = {
      border = "single", -- none, single, double, shadow
      position = "bottom", -- bottom, top
    },
  }

  local opts = {
    mode = "n", -- Normal mode
    prefix = "<leader>",
    buffer = nil, -- Global mappings. Specify a buffer number for buffer local mappings
    silent = true, -- use `silent` when creating keymaps
    noremap = true, -- use `noremap` when creating keymaps
    nowait = false, -- use `nowait` when creating keymaps
  }

  local visualOpts = {
    mode = "v", -- Visual mode
    prefix = "<leader>",
    buffer = nil, -- Global mappings. Specify a buffer number for buffer local mappings
    silent = true, -- use `silent` when creating keymaps
    noremap = true, -- use `noremap` when creating keymaps
    nowait = false, -- use `nowait` when creating keymaps
  }

  -- Map all keybindings here that start with a leader key
  local mappings = {
    f = {":Telescope live_grep<CR>", "Search text, ignore .gitignore files"},
    l = {":set hls!<CR>", "Toggle searched highlighted text"},
    w = {":q<CR>", "Close current tab"},
    y = {'"+y', 'Yank to clipboard'},
    p = {'"+p', 'Paste from clipboard after the text'},
    P = {'"+P', 'Paste from clipboard before the text'},
    b = {":Telescope buffers<CR>", "Show open buffers"},
    c = {
      name = "Compare Splits",
      s = {":windo :diffthis<CR>", "Start compare"},
      t = {":windo diffoff<CR>", "Stop compare"}
    },

    o = {
      name = "Open Files",
      g = {":Telescope git_files<CR>", "Browse throug the Git repo files"},
      f = {":Telescope find_files<CR>",  "Browse throug all of the currnet folder's files"}
    },

    s = {
      name = "Split or spell check",
      v = {":vsp<CR>", "Split vertically"},
      s = {":sp<CR>", "Split Horizontally"},
      p = {":set spell!<CR>", "Toggle spell check"}
    },
    
    m = {
      name = "Markdown",
      s = {":InstantMarkdownPreview<CR>", "Start Preview"},
      t = {":InstantMarkdownStop<CR>", "Stop Preview"}
    },

    g = {
      name = "Git",
      s = {":Telescope git_status<CR>", "Show git status"},
      b = {":Telescope git_branches<CR>", "switch to another branch"},
      f = {":Neogit kind=split<CR>", "Start Neogit"},
      l = {":Neogit log<CR>", "Git Log"},
      d = {
        name = "Diff operations",
        o = {":DiffviewOpen<CR>", "Open the git diff"},
        c = {":DiffviewClose<CR>", "Close the git diff"},
      }
    },
    r = {':call VrcQuery()<CR>', 'Execute the rest request'},
  }

  whichkey.setup(conf)
  whichkey.register(mappings, opts)
  whichkey.register(mappings, visualOpts)
end

return M
