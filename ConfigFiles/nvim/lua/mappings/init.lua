local map = vim.api.nvim_set_keymap
local opts = { noremap = true, silent = true}

vim.g.mapleader = ' '

-- Navigating between panes
map('n', '<C-h>', '<C-w>h', opts)
map('n', '<C-j>',  '<C-w>j', opts)
map('n', '<C-k>',  '<C-w>k', opts)
map('n', '<C-l>',  '<C-w>l', opts)

-- Prettify the JSON
map('n', '<C-j>', '<Cmd>%!python3 -m json.tool<CR><Cmd>set ft=json<CR>', opts)
map('v', '<C-j>', ':%!python3 -m json.tool<CR>', opts)

-- Toggle the COC explorer
map('n', '<C-n>', '<Cmd>CocCommand explorer<CR>', opts)

-- Floaterm
map('n', '<F2>', ':FloatermToggle<CR>', opts)
map('t', '<F2>',  '<C-\\><C-n>:FloatermToggle<CR>', opts)
