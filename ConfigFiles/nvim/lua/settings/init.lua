local set = vim.opt

-- Colorscheme
set.background = dark

-- Tabs configuration
set.tabstop = 2
set.shiftwidth = 2
set.expandtab = true

-- In text search
set.hls = true -- highlights the search
set.is = true  -- highlights the next search result while typing

-- Visual settings
set.number = true
set.rnu = true

-- Split the right way
set.spr = true 
set.sb = true

-- Spelling
set.spelllang = {'en_us'}

-- Time in milliseconds to wait for a mapped sequence to complete.
set.timeoutlen = 500

