# PATH
set -e fish_user_paths
set -U fish_user_paths $HOME/.bin  $HOME/.local/bin $fish_user_paths

# Enable starship
if type -q starship
  starship init fish | source
end

# Env vars
set -gx EDITOR "nvim"
set -gx VISUAL "nvim"

# Functions

# Aliases
## Abreviations
if [ -n "$HOSTNAME" ]
  abbr -a dbc distrobox-host-exec distrobox-create
  abbr -a dbe distrobox-host-exec distrobox-enter
else
  # Distrobox
  abbr -a dbc distrobox-create
  abbr -a dbe distrobox-enter
  abbr -a dbst distrobox-stop
  abbr -a dbd distrobox-rm
end

## Aliases
alias vim='nvim'
alias l='lazydocker'
if [ -n "$HOSTNAME" ]
  # Ubuntu host
  alias u-upd="distrobox-host-exec sudo apt update; distrobox-host-exec sudo apt upgrade"
  # Distrobox
  alias podman="distrobox-host-exec podman"
  alias dbs='distrobox-host-exec distrobox-enter "$(podman ps -a --format "{{.Names}}" | fzf-tmux -p --reverse)"'
  alias dbt='distrobox-host-exec distrobox-stop "$(podman ps -a --format "{{.Names}}" | fzf-tmux -p --reverse)"'
  alias dbd='distrobox-host-exec distrobox-rm "$(podman ps -a --format "{{.Names}}" | fzf-tmux -p --reverse)"'
else
  # Ubuntu host
  alias u-upd="sudo apt update; sudo apt upgrade"
end

if test -e ~/.config/fish/se_config.fish
  source ~/.config/fish/se_config.fish
end

if status is-interactive
  # Commands to run in interactive sessions can go here
  # Enable Atuin
  if type -q atuin
    atuin init fish | source
  end
end
