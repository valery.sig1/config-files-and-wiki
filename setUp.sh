#!/usr/bin/env bash
# Set up config files
## Alacritty
mkdir -p ~/.config/alacritty
# ln -fs $PWD/ConfigFiles/alacritty.toml ~/.config/alacritty/alacritty.toml
ln -fs $PWD/ConfigFiles/alacritty.yaml ~/.config/alacritty/alacritty.yaml
## Tmux
ln -fs $PWD/ConfigFiles/.tmux.conf ~/.tmux.conf

## NeoVim
ln -fs $PWD/ConfigFiles/nvim ~/.config/nvim

## Fish
ln -fs $PWD/ConfigFiles/config.fish ~/.config/fish/config.fish

## Starship
ln -fs $PWD/ConfigFiles/starship.toml ~/.config/starship.toml

## VIFM
mkdir -p ~/.config/vifm
ln -fs $PWD/ConfigFiles/vifm/vifmrc ~/.config/vifm/vifmrc

## IntelliJ .vimrc
ln -fs $PWD/ConfigFiles/.ideavimrc ~/.ideavimrc

## Rofi
ln -fs $PWD/ConfigFiles/rofi ~/.config/rofi
