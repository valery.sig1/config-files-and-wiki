# Config files
[[_TOC_]]
## Set up on a Linux/Mac system
**Fist of all note that all of the following files in your system will be overwritten**<BR>
Then simply run the following commands:
```bash
chmod +x setUp.sh
./setUp.sh
```
## Config files
All config file paths are the same as they should be in your Linux OS, relative to your home folder (~)
There are currently config files for the following programms:
### Fish shell
[Homepage](https://fishshell.com/docs/current/tutorial.html)

### Starship prompt
[Homepage](https://starship.rs/)

### NeoVim
[Homepage](http://github.com/neovim/neovim)
* [init.lua](ConfigFiles/nvim/init.lua)
* [coc-settings.json](ConfigFiles/coc-settings.json)

#### Perliminary steps:
* First intall the [Lua Programming language](https://www.lua.org/download.html)
* Then you will need to install the [Lazy plugin manager](https://github.com/folke/lazy.nvim/)
#### COC explorer
[Homepage](https://github.com/weirongxu/coc-explorer)
After you add the plugin you need to install the COC explorer with the following command: `:CocInstall coc-explorer`
#### VIM Telescope
[Homepage](https://github.com/nvim-telescope/telescope.nvim)
You will need to install a couple of dependencies:
1. [fd](https://github.com/sharkdp/fd)
2. [ripgrep](https://github.com/BurntSushi/ripgrep)

#### Instant Markdown
[Homepage](https://github.com/instant-markdown/vim-instant-markdown)
You will need to do the following steps:
* Install `nodejs`
* Run the following command `npm -g install instant-markdown-d`
### Alacritty (Terminal emulator)
[Homepage](http://github.com/alacritty/alacritty)<BR>
Please note that if you wish to use the specified fonts in the following config file, please download them [here](https://github.com/romkatv/powerlevel10k#meslo-nerd-font-patched-for-powerlevel10k)
* [alacritty.yml](ConfigFiles/alacritty.yml)
### Atuin
[Homepage](https://github.com/atuinsh/atuin)
### Tmux
[Homepage](http://github.com/tmux/tmux/wiki)
* [.tmux.conf](ConfigFiles/.tmux.conf)
#### Enable the powerline theme
The source command is already in the `.tmux.conf` file<BR>
* You will need the `python3-pip` for this, so if you don't have it, please install it:
  * `sudo apt install python3-pip` (For Ubuntu)
* All you have to do is install the `powerline-status` python package and you are good to go:
  * `sudo pip3 install powerline-status`
### VIFM
[Homepage](https://github.com/vifm/vifm)
### Rofi
[Homepage](https://github.com/davatorium/rofi)
### Enable the fonts - If you are using `fish` skip this step
* Please add the following lines to your `.zshrc` or `.profile` file:
```sh
export LANG=en_US.UTF-8
export LC_CTYPE=en_US.UTF-8
```
